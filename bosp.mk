
ifdef CONFIG_EXTERNAL_OPENCL

# Targets provided by this project
.PHONY: opencl clean_opencl

INSTALL_RUNTIMES = 
CLEAN_RUNTIMES   = 
ifdef CONFIG_EXTERNAL_OPENCL_AMD
  INSTALL_RUNTIMES += install_amd 
  CLEAN_RUNTIMES   += clean_amd
endif
ifdef CONFIG_EXTERNAL_OPENCL_NVIDIA
  INSTALL_RUNTIMES += install_nvidia 
  CLEAN_RUNTIMES   += clean_nvidia
endif
ifdef CONFIG_EXTERNAL_OPENCL_INTEL
  INSTALL_RUNTIMES += install_intel
  CLEAN_RUNTIMES   += clean_intel
endif

# Add this to the "contrib_testing" target
external: opencl
clean_external: clean_opencl

MODULE_DIR_opencl=$(BASE_DIR)/external/optional/opencl
OPENCL_BASEURL=http://bosp.dei.polimi.it/bosp/OpenCL

opencl: setup install_opencl

install_opencl: $(INSTALL_RUNTIMES)
	@ln -sf $(OPENCL_ROOT)/include/CL $(BASE_DIR)/out/include/

clean_opencl: $(CLEAN_RUNTIMES)
	@echo
	@echo "All OpenCL run-times removed"

################################################################################
# AMD RunTime Installer
################################################################################
ifdef CONFIG_EXTERNAL_OPENCL_AMD

OPENCL_ROOT=/opt/AMDAPP
AMD_TEMP=/tmp/BOSPAMD
AMD_INSTALLER=$(MODULE_DIR_opencl)/amd_install.sh
AMD_VER=32
ifeq "$(MACH)" "x86_64"
	AMD_VER=64
endif
AMD_PACKAGE_NAME=AMD-APP-SDK-v2.9-lnx$(AMD_VER).tgz
AMD_PACKAGE=$(AMD_TEMP)/$(AMD_PACKAGE_NAME)

download_opencl: download_amd

PHONY: download_amd
download_amd: $(AMD_PACKAGE)
$(AMD_PACKAGE): $(AMD_INSTALLER)
	@echo "Downloading AMD APP SDK $(MACH) installer..."
	@[ -d $(AMD_TEMP) ] || mkdir $(AMD_TEMP)
	@[ -f $(AMD_PACKAGE) ] || \
		curl $(OPENCL_BASEURL)/$(AMD_PACKAGE_NAME) -o $(AMD_PACKAGE)

$(AMD_PACKAGE): download_amd

$(AMD_INSTALLER): $(AMD_PACKAGE)

PHONY: install_amd
install_amd: $(OPENCL_ROOT)/include/CL/cl.h
$(OPENCL_ROOT)/include/CL/cl.h:
	#$(AMD_INSTALLER)
	@echo "Installing AMD APP SDK $(MACH)..."
	@cd $(AMD_TEMP) && tar xzf $(AMD_PACKAGE_NAME)
	@cd $(AMD_TEMP) && sudo $(MODULE_DIR_opencl)/amd_install.sh
	@touch $(AMD_INSTALLER)
	@touch $(AMD_PACKAGE)
	@sudo touch $@

PHONY: clean_amd
clean_amd:
	@sudo rm -rf $(AMD_TEMP)

endif # CONFIG_EXTERNAL_OPENCL_AMD

################################################################################
# ARM
################################################################################
ifdef CONFIG_EXTERNAL_OPENCL_ARM

install_opencl:
	@echo "Using ARM OpenCL SDK:"
	@echo ${CONFIG_EXTERNAL_OPENCL_ROOT}
	@cp -r ${CONFIG_EXTERNAL_OPENCL_ROOT}/lib/libOpenCL.so $(BUILD_DIR)/lib
	@cp -r ${CONFIG_EXTERNAL_OPENCL_ROOT}/include/CL $(BUILD_DIR)/include


endif # CONFIG_EXTERNAL_OPENCL_ARM

################################################################################
# Intel RunTime Installer
################################################################################
ifdef CONFIG_EXTERNAL_OPENCL_INTEL

PHONY: install_intel
install_intel:
	@echo "Intel OpenCL should be already installed in the system..."

endif # CONFIG_EXTERNAL_OPENCL_INTEL

################################################################################
# NVidia RunTime Installer
################################################################################
ifdef CONFIG_EXTERNAL_OPENCL_NVIDIA

PHONY: install_nvidia
install_nvidia:
	@echo "NVidia OpenCL should be already installed in the system..."

endif # CONFIG_EXTERNAL_OPENCL_NVIDIA


################################################################################
# CMake support for build time OpenCL
################################################################################
# Update BOSP building system CMake common options
CMAKE_COMMON_OPTIONS += -DOPENCL_ROOT_DIR=$(OPENCL_ROOT)
CMAKE_COMMON_OPTIONS += -DOPENCL_INCLUDE_DIR=$(OPENCL_ROOT)/include
CMAKE_COMMON_OPTIONS += -DOPENCL_LIBRARY_DIR=$(OPENCL_ROOT)/lib/$(MACH)

else # CONFIG_EXTERNAL_OPENCL

opencl:
	$(warning external opencl module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_OPENCL

